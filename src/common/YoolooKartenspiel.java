// History of Change
// vernr    |date  | who | lineno | what
//  V0.106  |200107| cic |    -   | add history of change

package common;

import java.io.*;
import java.util.*;
import java.util.logging.*;
import java.util.ArrayList;
import java.util.Random;

import server.*;
import stats.*;


public class YoolooKartenspiel {
	
	public enum Kartenfarbe {
		Gelb, Rot, Gruen, Blau, Orange, Pink, Violett, Tuerkis
	}

	private String Spielname = "Yooloo";

	private Map<Integer, String> winningPositions = new HashMap<Integer, String>();
	private TreeMap<Integer, String> tree = new TreeMap<Integer, String>();

	public final static int minKartenWert = 1;
	public final static int maxKartenWert = 10;

	protected YoolooKarte[][] spielkarten;
	protected int anzahlFarben = YoolooKartenspiel.Kartenfarbe.values().length;
	protected int anzahlWerte = maxKartenWert;
	ArrayList<YoolooSpieler> spielerliste = new ArrayList<YoolooSpieler>();
        
        //Cheatmode Variablen
        protected int erlaubteCheats = 0;
        public int[] verfügbareCheats;
	
	/**
	 * Erstellen einer neuen Spielumgebung Definition des Spielnamens der
	 * Spielkarten
	 */
	public YoolooKartenspiel(boolean isOnServer) {

		setSpielname("Yooloo" + System.currentTimeMillis());
		if( isOnServer ) {
			YoolooServer.LOGGER.log(Level.INFO, "[YoolooKartenSpiel] Spielname: " + getSpielname());
		}
		else {
			System.out.println("[YoolooKartenSpiel] Spielname: " + getSpielname()); // TODO evtl loeschen
		}

		spielerliste.clear();
		spielkarten = new YoolooKarte[anzahlFarben][anzahlWerte];

		for (int farbe = 0; farbe < anzahlFarben; farbe++) {
			for (int wert = 0; wert < anzahlWerte; wert++) {
				spielkarten[farbe][wert] = new YoolooKarte(Kartenfarbe.values()[farbe], (wert + 1));
			}
		}

		if( isOnServer ) {
			YoolooServer.LOGGER.log(Level.INFO, "Je " + anzahlWerte + " Spielkarten fuer " + anzahlFarben + " Spieler zeugt");
		}
		else {
			System.out.println("Je " + anzahlWerte + " Spielkarten fuer " + anzahlFarben + " Spieler zeugt");
		}
	}

	public void listeSpielstand() {
		if (spielerliste.isEmpty()) {
			System.out.println("(Noch) Keine Spieler registriert");
			//YoolooServer.LOGGER.log(Level.INFO, "(Noch) Keine Spieler registriert");
		} else {
			for (YoolooSpieler yoolooSpieler : spielerliste) {
				System.out.println(yoolooSpieler.toString());
				//YoolooServer.LOGGER.log(Level.INFO, yoolooSpieler.toString());
			}
		}

	}

	//update stats
	public void updateStats() throws IOException  {
		try{

			if (spielerliste.isEmpty()) {
				System.out.println("Error, keine Spieler registriert");
				System.exit(1);
			} else {
				for (YoolooSpieler yoolooSpieler : spielerliste) {
					winningPositions.put(yoolooSpieler.getPunkte(), yoolooSpieler.getName());
				}
				tree.putAll(winningPositions);
				Integer loopcount = 0;
				for(Map.Entry<Integer,String> entry : tree.entrySet()) {
					Integer key = entry.getKey();
					String value = entry.getValue();

					StatsWizard sw = new StatsWizard();
					System.out.println("Spieler: "+value+" hat "+key+" Punkte erreicht!");
					sw.writePlayerStats(value, key, (tree.size() - (loopcount)) , tree.size());
					loopcount++;
				}

			}
		}
		catch(IOException exception){
			System.out.println(
					exception
			);
		}
	}

	/**
	 * Luesst einen neuen Spieler an dem Spiel teilnehmen /Fuer lokale Simulationsmit
	 * mit Zuordnung der Farbe
	 * 
	 * @param name
	 * @return
	 */
	public YoolooSpieler spielerRegistrieren(String name) {
		YoolooSpieler neuerSpieler = new YoolooSpieler(name, maxKartenWert);
		Kartenfarbe[] farben = Kartenfarbe.values();
		neuerSpieler.setSpielfarbe(farben[spielerliste.size()]);
		YoolooKarte[] spielerkarten = spielkarten[spielerliste.size()];
		neuerSpieler.setAktuelleSortierung(spielerkarten);
		this.spielerliste.add(neuerSpieler);
		System.out.println("Debug; Spieler " + name + " registriert als : " + neuerSpieler);
		//YoolooServer.LOGGER.log(Level.INFO, "Spieler " + name + " registriert als : " + neuerSpieler);
		return neuerSpieler;
	}

	/**
	 * Luesst ein YoolooSpieler aus ClientServer Variante an dem Spiel teilnehmen Der
	 * Spieler erhuelt die Farbe korrespondierend zur ClientHandlerID
	 * 
	 * @param spielerName
	 * @return
	 */
	public YoolooSpieler spielerRegistrieren(YoolooSpieler neuerSpieler) {
		Kartenfarbe[] farben = Kartenfarbe.values();
		neuerSpieler.setSpielfarbe(farben[neuerSpieler.getClientHandlerId()]);
		YoolooKarte[] kartenDesSpielers = spielkarten[neuerSpieler.getClientHandlerId()];
		neuerSpieler.setAktuelleSortierung(kartenDesSpielers);
		this.spielerliste.add(neuerSpieler); // nur fuer Simulation noetig!
		//System.out.println("Debug; Spielerobject registriert als : " + neuerSpieler);
		YoolooServer.LOGGER.log(Level.INFO, "Spielerobject registriert als : " + neuerSpieler);
		return neuerSpieler;
	}

	@Override
	public String toString() {
		return "YoolooKartenspiel [anzahlFarben=" + anzahlFarben + ", anzahlWerte=" + anzahlWerte + ", getSpielname()="
				+ getSpielname() + "]";
	}

	// nur fuer Simulation / local
	public void spielerSortierungFestlegen() {
		for (int i = 0; i < spielerliste.size(); i++) {
			spielerliste.get(i).sortierungFestlegen();
		}
	}

	// nur fuer Simulation / local
	public void spieleRunden() {
		// Schleife ueber Anzahl der Karten
		for (int i = 0; i < anzahlWerte; i++) {
			System.out.println("Runde " + (i + 1));
			// Schleife ueber Anzahl der Spieler
			YoolooKarte[] stich = new YoolooKarte[spielerliste.size()];

                        
			for (int j = 0; j < spielerliste.size(); j++) {
				YoolooKarte aktuelleKarte = spielerliste.get(j).getAktuelleSortierung()[i];
                                
                                //Eine ArrayList mit den Indexen der höheren Karten (Nur darauffolgende)
                                ArrayList<Integer> höhereKarten = new ArrayList();

                                //Cheatmode Prüfung auf eine höhere Karte
                                for(int g = i; g < spielerliste.get(j).getAktuelleSortierung().length; g++){
                                    if(spielerliste.get(j).getAktuelleSortierung()[g].getWert() > aktuelleKarte.getWert()){
                                        //System.out.println("Der Spieler hat eine Höhere Karte mit Index: "+ g + " mit dem Wert: " +spielerliste.get(j).getAktuelleSortierung()[g].getWert());
                                        //Erstelle eine Liste mit den Indizes der höheren Karten auf dem Deck, welche noch nicht gespielt wurden
                                        höhereKarten.add(g);
                                    }
                                }
                                System.out.println("Der Spieler hat noch " + verfügbareCheats[j] + " Cheats zur Verfügung!");
                                
                                //sollte der Spieler noch Cheats verfügbar haben, und auch noch Höhere Karten auf dem Deck
                                if(verfügbareCheats[j] > 0 && !höhereKarten.isEmpty()){
                                    Random random = new Random();
                                    //Zufällige Entscheidung ob die Person tatsächlich cheatet oder nicht
                                    int useCheat = random.nextInt(2);
                                    
                                    
                                    //Sollte er sich für das Cheaten entscheiden
                                    if (useCheat == 1){
                                        System.out.print(spielerliste.get(j).getName() + " möchte cheaten! \n");
                                        
                                        //Hole das aktuelle KartenSet
                                        YoolooKarte[] aktuellesSet = spielerliste.get(j).getAktuelleSortierung();
//                                        for(YoolooKarte karte : spielerliste.get(j).getAktuelleSortierung()){
//                                            System.out.println("aktuelles Set" + karte);
//                                        }
                                        
                                        
                                        //Speichere die aktuelle Karte des Decks in einer temporären Variable
                                        YoolooKarte temp = aktuellesSet[i];
                                        YoolooKarte getauschteKarte = aktuellesSet[i];
                                        
                                        //Überschreibe die aktuelle Karte mit der ausgewählten, höheren Karte und ersetze die Stelle der ausgewählten mit der Temporären
                                        int cheatKarteIndex = random.nextInt(höhereKarten.size());
//                                        System.out.println("cheatKarteIndex: " +cheatKarteIndex);
                                        
                                        YoolooKarte getauschteKarte2 = aktuellesSet[höhereKarten.get(cheatKarteIndex)];
                                        aktuellesSet[i] = aktuellesSet[höhereKarten.get(cheatKarteIndex)];
                                        aktuellesSet[höhereKarten.get(cheatKarteIndex)] = temp;
                                        
//                                        for(YoolooKarte karte : spielerliste.get(j).getAktuelleSortierung()){
//                                            System.out.println("neues Set" + karte);
//                                        }
                                        
                                        System.out.println("Karte : " + getauschteKarte.toString()+ " wurde getauscht mit: "+ getauschteKarte2.toString());
                                        //Überschreibe das alte KartenSet mit dem neuen
                                        spielerliste.get(j).setAktuelleSortierung(aktuellesSet);
                                        verfügbareCheats[j]--;
                                    }
                                    else {
                                        System.out.print(spielerliste.get(j).getName() + " hat sich gegen das Cheaten entschieden!");
                                    }
                                        
                                }
                                
                                
                                aktuelleKarte = spielerliste.get(j).getAktuelleSortierung()[i];
				stich[j] = aktuelleKarte;
				System.out.println(spielerliste.get(j).getName() + " spielt " + aktuelleKarte.toString());
			}
			int stichgewinner = berechneGewinnerIndex(stich, false);
			if (stichgewinner>=0) {
				spielerliste.get(stichgewinner).erhaeltPunkte(i + 1, false);
			}
		}
	}

	public int berechneGewinnerIndexV1_Buggy(YoolooKarte[] karten) {

		int limitWert = maxKartenWert + 1;
		int maxWert = 0;
		int anzahlKartenMitMaxWert = 0;
		String output = "";
		for (int i = 0; i < karten.length; i++) {
			//System.out.print(i + ":" + karten[i].getWert() + " ");
			output += i + ":" + karten[i].getWert() + " ";
		}
		//System.out.println();
		output += "\n";
		YoolooServer.LOGGER.log(Level.INFO, output);
		while (anzahlKartenMitMaxWert != 1) {
			maxWert = 0;
			for (int i = 0; i < karten.length; i++) {
				YoolooKarte yoolooKarte = karten[i];
				if (maxWert < yoolooKarte.getWert() && yoolooKarte.getWert() < limitWert) {
					maxWert = yoolooKarte.getWert();
				}
			}
			anzahlKartenMitMaxWert = 0;
			for (int i = 0; i < karten.length; i++) {
				YoolooKarte yoolooKarte = karten[i];
				if (maxWert == yoolooKarte.getWert()) {
					anzahlKartenMitMaxWert++;
					limitWert = yoolooKarte.getWert();
				}
			}
			if (limitWert == 0 && anzahlKartenMitMaxWert == 0) {
				return -1;
			}
		}
		int gewinnerIndex = -1;
		for (int i = 0; i < karten.length; i++) {
			YoolooKarte yoolooKarte = karten[i];
			if (yoolooKarte.getWert() == maxWert) {
				gewinnerIndex = i;
			}
		}
		return gewinnerIndex;
	}


	public int berechneGewinnerIndex(YoolooKarte[] karten, boolean isOnServer) {
		int maxwert = 0;
		String output = "";
		for (int i = 0; i < karten.length; i++) {
			//System.out.print(i + ":" + karten[i].getWert() + " ");
			output += i + ":" + karten[i].getWert() + " ";
			if (maxwert < karten[i].getWert()) {
				maxwert = karten[i].getWert();
			}
		}
		int gewinnerIndex = -1;
		while (maxwert > 0) {
			int anzahlKartenMitMaxwert = 0;
			for (int i = 0; i < karten.length; i++) {
				if (karten[i].getWert() == maxwert) {
					anzahlKartenMitMaxwert++;
					gewinnerIndex = i;
				}
			}
			if (anzahlKartenMitMaxwert != 1) {
				maxwert--;
				gewinnerIndex = -1;

			} else {
				output += "gewinnerIndex: " + gewinnerIndex;
				if( isOnServer ) {
					YoolooServer.LOGGER.log(Level.INFO, output);
				}
				else {
					System.out.println(output);
				}
				return gewinnerIndex;
			}
		}
		output += "Kein gewinnerIndex: ermittelt" + gewinnerIndex;
		if( isOnServer ) {
			YoolooServer.LOGGER.log(Level.INFO, output);
		}
		else {
			System.out.println(output);
		}
		return gewinnerIndex;
	}
	
        
        //Setze den Cheatmode auf (int cheats) Cheatversuche ( > 0 = Aktiviert )
        public void setCheatmode(int cheats){
            this.erlaubteCheats = cheats;
            System.out.println(cheats == 0 ? "Cheatmode ist DEAKTIVIERT!" : "Cheatmode wurde aktiviert!");
            initializeCheatVersuche(cheats);
        }
        
        //Setze für jeden Player die Cheatversuche auf die angegebene Anzahl (int cheats)
        public void initializeCheatVersuche(int cheats){
            this.verfügbareCheats = new int[this.spielerliste.size()];
            
            for(int i = 0; i < verfügbareCheats.length; i++){
                verfügbareCheats[i] = cheats;
            }
        }
        
   public String getSpielname() {
   return Spielname;
}

public void setSpielname(String spielname) {
   Spielname = spielname;
}
 

}
