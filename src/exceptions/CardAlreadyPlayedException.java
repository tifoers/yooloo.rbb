package exceptions;

import common.YoolooKarte;
import common.YoolooSpieler;

public class CardAlreadyPlayedException extends RuntimeException{
	
	public CardAlreadyPlayedException(YoolooKarte card,YoolooSpieler player){
		
		System.out.println("The player: " + player.getName() + "tried to play card: " + card.getWert() + ", which was already played");
		
	}

}
