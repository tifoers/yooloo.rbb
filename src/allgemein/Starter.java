// History of Change
// vernr    |date  | who | lineno | what
//  V0.106  |200107| cic |    -   | add history of change

package allgemein;

import java.io.*;

import common.*;

public class Starter {
	public static void main(String[] args) throws IOException {


//		Initialisieren des Spiels 

		YoolooKartenspiel yooloo = new YoolooKartenspiel(false);
		yooloo.listeSpielstand();

//		Registrieren der Spieler
		yooloo.spielerRegistrieren("Juergen");
		yooloo.spielerRegistrieren("Reiner");
		yooloo.spielerRegistrieren("Benedikt");
		yooloo.spielerRegistrieren("Olaf");

                //Setzen der Cheatvariablen
                yooloo.setCheatmode(5);

                                
                                
//		Sortieren der Karten
		yooloo.spielerSortierungFestlegen();

//		Ausspielend der Runden
		yooloo.spieleRunden();

//		Spielstand anzeigen + stats updaten
		yooloo.listeSpielstand();
		yooloo.updateStats();
	}
}
