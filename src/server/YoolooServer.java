// History of Change
// vernr    |date  | who | lineno | what
//  V0.106  |200107| cic |    -   | add history of change 

package server;

import java.io.*;
import java.net.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.logging.*;

import common.*;

public class YoolooServer {

	//Logger
	public static final Logger LOGGER = Logger.getLogger("ServerLogger");

	//Handler f�r die Log File und die Konsolen Logs
	FileHandler fh;
	ConsoleHandler ch;

	// Server Standardwerte koennen ueber zweite Konstruktor modifiziert werden!
	private int port = 44137;
	private int spielerProRunde = 8; // min 1, max Anzahl definierte Farben in Enum YoolooKartenSpiel.KartenFarbe)
	private GameMode serverGameMode = GameMode.GAMEMODE_SINGLE_GAME;

	public GameMode getServerGameMode() {
		return serverGameMode;
	}

	public void setServerGameMode(GameMode serverGameMode) {
		this.serverGameMode = serverGameMode;
	}

	private ServerSocket serverSocket = null;
	private boolean serverAktiv = true;

	// private ArrayList<Thread> spielerThreads;
	private ArrayList<YoolooClientHandler> clientHandlerList;

	private ExecutorService spielerPool;

	/**
	 * Serverseitig durch ClientHandler angebotenen SpielModi. Bedeutung der
	 * einzelnen Codes siehe Inlinekommentare.
	 * 
	 * Derzeit nur Modus Play Single Game genutzt
	 */
	public enum GameMode {
		GAMEMODE_NULL, // Spielmodus noch nicht definiert
		GAMEMODE_SINGLE_GAME, // Spielmodus: einfaches Spiel
		GAMEMODE_PLAY_ROUND_GAME, // noch nicht genutzt: Spielmodus: Eine Runde von Spielen
		GAMEMODE_PLAY_LIGA, // noch nicht genutzt: Spielmodus: Jeder gegen jeden
		GAMEMODE_PLAY_POKAL, // noch nicht genutzt: Spielmodus: KO System
		GAMEMODE_PLAY_POKAL_LL // noch nicht genutzt: Spielmodus: KO System mit Lucky Looser
	};

	public YoolooServer(int port, int spielerProRunde, GameMode gameMode) {
		this.port = port;
		this.spielerProRunde = spielerProRunde;
		this.serverGameMode = gameMode;
	}

	public void startServer() {
		try {
			// Init
			initLogging();
			serverSocket = new ServerSocket(port);
			spielerPool = Executors.newCachedThreadPool();
			clientHandlerList = new ArrayList<YoolooClientHandler>();
			//System.out.println("Server gestartet - warte auf Spieler");
			LOGGER.info("Server gestartet - warte auf Spieler");

			while (serverAktiv) {
				Socket client = null;

				// Neue Spieler registrieren
				try {
					client = serverSocket.accept();
					YoolooClientHandler clientHandler = new YoolooClientHandler(this, client);
					clientHandlerList.add(clientHandler);
					//System.out.println("[YoolooServer] Anzahl verbundene Spieler: " + clientHandlerList.size());
					LOGGER.log(Level.INFO, "[YoolooServer] Anzahl verbundene Spieler: " + clientHandlerList.size());
				} catch (IOException e) {
					//System.out.println("Client Verbindung gescheitert");
					//e.printStackTrace();
					LOGGER.log(Level.SEVERE, "Client Verbindung gescheitert", e);
				}

				// Neue Session starten wenn ausreichend Spieler verbunden sind!
				if (clientHandlerList.size() >= Math.min(spielerProRunde,
						YoolooKartenspiel.Kartenfarbe.values().length)) {
					// Init Session
					YoolooSession yoolooSession = new YoolooSession(clientHandlerList.size(), serverGameMode);

					// Starte pro Client einen ClientHandlerTread
					for (int i = 0; i < clientHandlerList.size(); i++) {
						YoolooClientHandler ch = clientHandlerList.get(i);
						ch.setHandlerID(i);
						ch.joinSession(yoolooSession);
						spielerPool.execute(ch); // Start der ClientHandlerThread - Aufruf der Methode run()
					}

					// nuechste Runde eroeffnen
					clientHandlerList = new ArrayList<YoolooClientHandler>();
				}
			}
		} catch (IOException e1) {
			//System.out.println("ServerSocket nicht gebunden");
			//e1.printStackTrace();
			LOGGER.log(Level.SEVERE, "ServerSocket nicht gebunden", e1);
			serverAktiv = false;
		}

	}

	// TODO Dummy zur Serverterminierung noch nicht funktional
	public void shutDownServer(int code) {
		if (code == 543210) {
			this.serverAktiv = false;
			//System.out.println("Server wird beendet");
			LOGGER.log(Level.INFO, "Server wird beendet");
			spielerPool.shutdown();
		} else {
			//System.out.println("Servercode falsch");
			LOGGER.log(Level.WARNING, "Servercode falsch");
		}
	}


	public void initLogging() throws SecurityException, IOException {
		SimpleFormatter formatter = new SimpleFormatter() {
			private static final String format = "[%1$s] [%2$-7s] %3$s %n";
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

			@Override
			public synchronized String format(LogRecord lr) {
				return String.format(
						format,
						dateFormat.format(new Date(lr.getMillis())),
						lr.getLevel().getLocalizedName(),
						lr.getMessage());
			}
		};
		fh = new FileHandler(System.getProperty("user.dir") + "/src/server/serverlog.log");
		fh.setFormatter(formatter);
		ch = new ConsoleHandler();
		ch.setFormatter(formatter);
		LogManager.getLogManager().reset();
		LOGGER.addHandler(fh);
		LOGGER.addHandler(ch);
	}
}
