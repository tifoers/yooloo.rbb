package stats;
import java.io.*;

import com.google.gson.*;
public class StatsWizard {

    public StatsWizard() {

    }

    public Object readStatsOfPlayer(String playerName) throws IOException {
        playerName = playerName.toLowerCase();
        BufferedReader br;
        try{
            br = new BufferedReader(new FileReader("src/stats/players/"+playerName+".json"));

        }catch(FileNotFoundException e){
            // create empty player model
            StatsModel dummy = new StatsModel();
            dummy.name = playerName; // set name to playerName
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            String json = gson.toJson(dummy);

            // fill empty file with dummy data
            try(PrintWriter out = new PrintWriter("src/stats/players/"+playerName+".json")){
                out.println(json);
            }
        } finally{

            // get complete String
            br = new BufferedReader(new FileReader("src/stats/players/"+playerName+".json"));
            StringBuilder sb = new StringBuilder();
            String everything = "";
            try {
                String line = br.readLine();
                while (line != null) {
                    sb.append(line);
                    sb.append(System.lineSeparator());
                    line = br.readLine();
                }
                everything = sb.toString();


            }
            catch(java.io.IOException exception){
                System.out.println("EXCEPTION:");
                System.out.println(exception);
                System.exit(1);
            }
            finally {
                br.close();
                //build stats object out of json string and return it
                GsonBuilder builder = new GsonBuilder().setLenient();
                builder.setPrettyPrinting();
                Gson gson = builder.create();
                StatsModel stats = gson.fromJson(everything, StatsModel.class);
                return stats;
            }
        }

    }

    public void writePlayerStats(String playerName,int wonPointsThisRound, int playerPosition, int playerCountCurrentGame) throws IOException{
        playerName = playerName.toLowerCase();

        // get current stats
        StatsModel currentPlayer = (StatsModel) readStatsOfPlayer(playerName);

        // calculate the stats
        ++currentPlayer.playedGames;
        currentPlayer.totalPositionSum += playerPosition;
        currentPlayer.totalPlayersInAllGames += playerCountCurrentGame;
        currentPlayer.avgPlayersPerGame = (currentPlayer.totalPlayersInAllGames / currentPlayer.playedGames);
        currentPlayer.wonPointsAbs += wonPointsThisRound;
        currentPlayer.wonPointsRel = ((float)currentPlayer.wonPointsAbs / (float)(currentPlayer.playedGames * 55) * 100);
        currentPlayer.highScorePoints = (currentPlayer.highScorePoints < wonPointsThisRound ? wonPointsThisRound : currentPlayer.highScorePoints);
        currentPlayer.highScorePosition = (currentPlayer.highScorePosition < playerPosition ? currentPlayer.highScorePosition : playerPosition);
        currentPlayer.wonGames = (playerPosition == 1 ? ++currentPlayer.wonGames : currentPlayer.wonGames );
        currentPlayer.avgPosition = ((float)currentPlayer.totalPositionSum / (float) currentPlayer.playedGames);
        currentPlayer.lastGamePoints = wonPointsThisRound;



        //write new stats to same file
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String json = gson.toJson(currentPlayer);
        System.out.println(json);
        try(PrintWriter out = new PrintWriter("src/stats/players/"+playerName+".json")) {
            out.println(json);
        }
        catch(IOException e){
            System.out.println(e);
        }

    }
}
