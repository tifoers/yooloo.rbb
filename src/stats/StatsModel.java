package stats;

public class StatsModel {
    public String name = null;
    public int playedGames = 0;
    public int avgPlayersPerGame = 0;
    public int wonPointsAbs = 0;
    public float wonPointsRel = 0;
    public int highScorePoints = 0;
    public int highScorePosition = 8;
    public int wonGames = 0;
    public float avgPosition = 0;
    public int totalPlayersInAllGames = 0;
    public int totalPositionSum = 0;
    public int lastGamePoints = 0;
}
